const { ApolloServer, gql } = require('apollo-server')


// resolver setup

const resolvers = {
    Query: {
        books: () => books
    }
}

// schema definition
const typeDefs = gql`
type book {
    title: String
    author: String
}
type Query {
    books: [book]
}`

// data set

const books = [
  {
    title: 'The Awakening',
    author: 'Kate Chopin',
  },
  {
    title: 'City of Glass',
    author: 'Paul Auster',
  },
];


// server setup
const apoloowServer = new ApolloServer({typeDefs, resolvers})

apoloowServer.listen().then(({url }) =>{
    console.log(`sserver ready at ${url}`)
})
